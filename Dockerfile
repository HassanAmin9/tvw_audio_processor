FROM hassanamin994/node_ffmpeg
WORKDIR /tvw_audio_processor

# Dont copy nodemodules over
RUN echo "node_modules" >> .dockerignore
COPY . .
RUN npm install

# ADD AWS CREDENTIALS FILE
ARG AWS_KEYS_FILE_BASE64
RUN mkdir ~/.aws
RUN echo ${AWS_KEYS_FILE_BASE64} | base64 --decode > ~/.aws/credentials

EXPOSE 4000
CMD [ "npm", "run", "docker:prod"]