
const async = require('async');
const fs = require('fs');
const audioProcessor = require('./audio_processor');
const utils = require('./utils');

const articleHandler = require('./dbHandlers/article');

const storageService = require('./vendors/storage');

const TRANSLATION_AUDIO_DIRECTORY = 'translation/audios';


function processAudio(articleId, slidePosition, subslidePosition, callback = () => { }) {
    console.log('starting for file', articleId, slidePosition, subslidePosition);
    articleHandler.findById(articleId).then((article) => {
        console.log(article)
        if (!article) {
            return callback(new Error('Invalid article voice id'));
        }

        updateAudioStatus(articleId, slidePosition, subslidePosition, { processing: true });

        const audio = article.slides.find((s) => parseInt(s.position) === parseInt(slidePosition)).content.find(s => parseInt(s.position) === parseInt(subslidePosition)).audio;
        const audioURL = audio.indexOf('https') === -1 ? `http:${audio}` : audio;
        console.log(audioURL)
        utils.getRemoteFile(audioURL, (err, filePath) => {
            if (err) {
                return callback(err);
            }

            const processingStepsFunc = [
                (cb) => {
                    const fileExtension = filePath.split('.').pop().toLowerCase();
                    if (fileExtension === 'mp3' || fileExtension === 'wav') return cb(null, filePath);
                    console.log('converint to wav');
                    audioProcessor.convertToWav(filePath, (err, outputPath) => {
                        if (err) {
                            console.log(err);
                            return cb(null, filePath);
                        }
                        fs.unlink(filePath, () => { })
                        return cb(null, outputPath);
                    })
                },
                (filePath, cb) => {
                    console.log('processing', filePath);
                    audioProcessor.clearBackgroundNoise(filePath, (err, outputPath) => {
                        fs.unlinkSync(filePath, () => { });
                        if (err) {
                            return cb(err);
                        }
                        return cb(null, outputPath)
                    })
                },
            ];

            async.waterfall(processingStepsFunc, (err, finalFilePath) => {
                console.log('Processed succesfully', err, audioURL);
                if (err || !fs.existsSync(finalFilePath)) return callback(err);

                storageService.saveFile(TRANSLATION_AUDIO_DIRECTORY, finalFilePath.split('/').pop(), fs.createReadStream(finalFilePath))
                    .then((uploadRes) => {
                        fs.unlink(finalFilePath, () => { });
                        return updateSubslide(articleId, slidePosition, subslidePosition, { audio: uploadRes.url, audioKey: uploadRes.data.Key, audioFileName: finalFilePath.split('/').pop() })
                    })
                    .then(() => {
                        // Delete old audio
                        const slide = article.slides.find((s) => s.position === slidePosition);
                        const subslide = slide.content.find((s) => s.position === subslidePosition);
                        if (subslide.audio && subslide.audioFileName) {
                            storageService.deleteFile(TRANSLATION_AUDIO_DIRECTORY, subslide.audioFileName)
                                .then(() => {
                                    console.log('deleted file');
                                })
                                .catch((err) => {
                                    console.log('error deleting file', err);
                                });
                        }
                        return callback(null, { success: true });
                    })
                    .catch(callback);
            })
        })
    })
        .catch(callback)
}


function updateAudioStatus(id, slidePosition, subslidePosition, { processing, status }) {
    // const updateObj = {};
    // if (processing !== undefined && processing !== 'undefined') {
    //   updateObj[`audios.${audioIndex}.processing`] = processing;
    // }
    // if (status) {
    //   updateObj[`audios.${audioIndex}.status`] = status;
    // }

    // Articl.findByIdAndUpdate(id, { $set: updateObj }, { new: true }, (err, res) => {
    // })
}



function updateSubslide(articleId, slidePosition, subslidePosition, changes) {
    return new Promise((resolve, reject) => {
        articleHandler.findById(articleId)
            .then((article) => {
                if (!article) return reject(new Error('Invalid article'));
                const { slides } = article;
                const slideIndex = slides.findIndex((s) => parseInt(s.position) === slidePosition);
                const contentIndex = slides[slideIndex].content.findIndex((s) => parseInt(s.position) === subslidePosition);
                let update = {}
                Object.keys(changes).forEach((key) => {
                    // slides[slideIndex].content[subslideIndex][key] = changes[key];
                    update[`slides.${slideIndex}.content.${contentIndex}.${key}`] = changes[key];
                })
                return articleHandler.updateById(article._id, { ...update });
            })
            .then(resolve)
            .catch(reject);
    })
}


module.exports = {
    processAudio,
    updateAudioStatus,
    updateSubslide,
}